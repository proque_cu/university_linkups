# Log analytics
[goaccess](https://goaccess.io/m)

### para varios files
``````
zcat -f 2016*_access* | goaccess --log-format=COMBINED --db-path /mnt/weblogs/goaccess/ -a > /mnt/weblogs/reporte.html
``````

### con rango de fechas
``````
for i in {09..10}; do ll 2016$i??*_access* | goaccess --log-format=COMBINED --db-path /mnt/weblogs/goaccess/ -a > /mnt/weblogs/reporte_rango.html; done
``````

# Self hosted cloud
[OpenStack](https://www.openstack.org/)